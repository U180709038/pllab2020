#include <stdio.h>
#include <stdlib.h>

int nrDigits(int num)
{
    static int count;
    if(num > 0)
    {
        count = count+1;
        nrDigits(num / 10);
    }
    else
        return count;
}

int main(void)
{
    int num;
    int count = 0;

    printf("Enter a positive integer number: ");
    scanf(" %d",&num);
    count = nrDigits(num);
    printf("Total digits in number %d is: %d\n",num,count);
    return(0);

}
