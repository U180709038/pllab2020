#include <stdio.h>

int gcd(int x, int y);

int main()
{
    int num1, num2, ans;

    printf("Enter 2 integers to fiind gcd: ");
    scanf("%d%d", &num1, &num2);

    ans = gcd(num1, num2);

    printf("GCD of %d and %d = %d", num1, num2, ans);

    return 0;
}

int gcd(int x, int y)
{
    if(y == 0)
        return x;
    else
        return gcd(y, x%y);
}
