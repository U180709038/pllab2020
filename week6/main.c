#include <stdio.h>
void stoogeSort(int [], int, int);

void main()
{
    int arr[100], i, n;

    printf("Enter the array size: ");
    scanf("%d", &n);
    printf("Enter 5 elements: ");
    scanf("%d", &i);
    for (i = 0;i < n; i++)
        scanf(" %d", &arr[i]);
    stoogeSort(arr, 0, n - 1);
    printf("Sorted array : \n");
    for (i = 0;i < n;i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}


void stoogeSort(int arr[], int i, int j)
{
    int temp, k;
    if (arr[i] > arr[j])
    {
        temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
    if ((i + 1) >= j)
        return;
    k = (int)((j - i + 1) / 3);
    stoogeSort(arr, i, j - k);
    stoogeSort(arr, i + k, j);
    stoogeSort(arr, i, j - k);
}
